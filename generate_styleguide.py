import re

NB_ICONS_PER_LINE = 12

with open("icons.html") as f:
    data = f.read()

pattern = r'id="[a-z0-9\-]+'
icons = [icon.replace("id=\"", "") for icon in re.findall(pattern, data)]
icons_styleguide = '\t<section class="container">\n\t\t<h3>Icons</h3>\n'
for idx, icon in enumerate(icons):
    if idx == 0:
        icons_styleguide += '\t\t<div class="row">\n\t\t\t<div class="boxes">\n'
    elif idx != len(icons) and idx % NB_ICONS_PER_LINE == 0:
        icons_styleguide += '\t\t\t</div>\n\t\t</div>\n\t\t<div class="row">\n\t\t\t<div class="boxes">\n'
    icons_styleguide += f'\t\t\t\t<div class="box">\n\t\t\t\t\t<svg class="icon xs color--black">\n\t\t\t\t\t\t<use href="#{icon}" viewbox="0 0 24 24"></use>\n\t\t\t\t\t</svg>\n\t\t\t\t\t<p>{icon}</p>\n\t\t\t\t</div>\n'
icons_styleguide += '\t\t\t</div>\n\t\t</div>\n'
icons_styleguide += '\t\t<h4>Usage:</h4>\n'
icons_styleguide += '\t\t<p>The following code snippet displays an icon:</p>\n'
icons_styleguide += '\t\t<pre>&lt;svg class="icon {xxs, xs, s, m, l, xl, or xxl} color--black"&gt;&lt;use href="#{id of the icon}" viewbox="0 0 24 24"&gt;&lt;/use&gt;&lt;/svg&gt;</pre>\n'
icons_styleguide += '\t</section>\n'

with open("variables.css") as f:
    data = f.read()

pattern = r'--[a-z0-9\-]+:'
variables = [variable.replace(":", "") for variable in re.findall(pattern, data)]
mid = int(len(variables) / 3)
variables_box_1 = "</li>\n\t\t\t\t\t\t<li>".join(variables[:mid])
variables_box_2 = "</li>\n\t\t\t\t\t\t<li>".join(variables[mid+1:2*mid])
variables_box_3 = "</li>\n\t\t\t\t\t\t<li>".join(variables[2*mid+1:])
variables_styleguide = '\t<section class="container">\n\t\t<h3>Variables</h3>\n\t\t<div class="row">\n\t\t\t<div class="boxes">\n'
variables_styleguide += f'\t\t\t\t<div class="box">\n\t\t\t\t\t<ul>\n\t\t\t\t\t\t<li>{variables_box_1}</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n'
variables_styleguide += f'\t\t\t\t<div class="box">\n\t\t\t\t\t<ul>\n\t\t\t\t\t\t<li>{variables_box_2}</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n'
variables_styleguide += f'\t\t\t\t<div class="box">\n\t\t\t\t\t<ul>\n\t\t\t\t\t\t<li>{variables_box_3}</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n'
variables_styleguide += '\t\t\t</div>\n\t\t</div>\n'
variables_styleguide += '\t\t<h4>Usage:</h4>\n'
variables_styleguide += '\t\t<p>The CSS variables can be used to replace hard coded values:</p>\n'
variables_styleguide += '\t\t<pre>var({name of the value})</pre>\n'
variables_styleguide += '\t</section>\n'

NB_COLORS_PER_LINE = 12
SQUARE_SIZE = 64

with open("colors.css") as f:
    data = f.read()

pattern = r'--[a-z0-9\-]+: .*;'
colors = [color.replace(";", "").split(": ") for color in re.findall(pattern, data)]
colors_styleguide = '\t<section class="container">\n\t\t<h3>Colors</h3>\n'
for idx, color in enumerate(colors):
    if idx == 0:
        colors_styleguide += '\t\t<div class="row">\n\t\t\t<div class="boxes">\n'
    elif idx != len(colors) and idx % NB_COLORS_PER_LINE == 0:
        colors_styleguide += '\t\t\t</div>\n\t\t</div>\n\t\t<div class="row">\n\t\t\t<div class="boxes">\n'
    colors_styleguide += f'\t\t\t\t<div class="box">\n\t\t\t\t\t<div style="width: {SQUARE_SIZE}px; height: {SQUARE_SIZE}px; background: {color[1]};"></div>\n\t\t\t\t\t<p>{color[0]}</p>\n\t\t\t\t</div>\n'
colors_styleguide += '\t\t\t</div>\n\t\t</div>\n'
colors_styleguide += '\t\t<h4>Usage:</h4>\n'
colors_styleguide += '\t\t<p>The CSS variables can be used to replace hard coded color:</p>\n'
colors_styleguide += '\t\t<pre>var({name of the value})</pre>\n'
colors_styleguide += '\t\t<p>The following CSS classes are also available:</p>\n'
colors_styleguide += '\t\t<pre>color{name of the color} background{name of the color}</pre>\n'
colors_styleguide += '\t</section>\n'

styleguide = f"""\
<section class="section">
\t<section class="container">
\t\t<h2>Icons and CSS variables</h2>
\t\t<p>→ <a href="https://gitlab.com/european-language-grid/platform/icons-and-css-variables">Link to the GitLab repository</a></p>
\t</section>
{icons_styleguide}
{variables_styleguide}
{colors_styleguide}
</section>
"""

with open("styleguide.html", "w") as f:
    f.write(styleguide)